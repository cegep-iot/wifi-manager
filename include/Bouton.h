#pragma once
#include <Arduino.h>
#include <WiFiManager.h>

class Bouton {
    private:
        uint8_t m_pin;
        int m_dernierEtatBouton;
        long m_derniereDateChangement;
        int m_dernierEtatStableBouton;
        const int m_delaiMinPression = 25;

    public:
        Bouton(int p_pinBouton);
        void tick();
};
