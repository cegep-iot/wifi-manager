#include "Arduino.h"
#include "Bouton.h"
#include <WiFiManager.h>

Bouton::Bouton(int p_pinBouton) : m_pin(p_pinBouton)
{
    pinMode(this->m_pin, INPUT);
    this->m_dernierEtatBouton = HIGH;
    this->m_derniereDateChangement = 0;
    this->m_dernierEtatStableBouton = HIGH;
}

void Bouton::tick()
{
    int etatBouton = digitalRead(this->m_pin);
    long dateActuelle = millis();

    if (etatBouton != this->m_dernierEtatBouton)
    {
        this->m_derniereDateChangement = dateActuelle;
        this->m_dernierEtatBouton = etatBouton;
    }

    if(dateActuelle - this->m_derniereDateChangement > this->m_delaiMinPression)
    {
        if (this->m_dernierEtatStableBouton == HIGH && etatBouton == LOW)
        {

        }
        else if (this->m_dernierEtatStableBouton == LOW && etatBouton == HIGH)
        {
            WiFiManager wm;
            wm.erase();
            ESP.restart();
            Serial.println("La configuration de connection a été reinitialisée.");
        }

        this->m_dernierEtatStableBouton = etatBouton;
    }
}